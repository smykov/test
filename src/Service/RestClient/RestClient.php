<?php

namespace App\Service\RestClient;

class RestClient implements RestClientInterface
{
    private $url, $login, $password;

    public function __construct(array $dataConnect)
    {
        $this->url = $dataConnect['url'];
        $this->login = $dataConnect['login'];
        $this->password = $dataConnect['password'];
    }

    public function connect()
    {
        return 'connect to rest client';
    }

    public function getScheme()
    {
        return 'get scheme rest client';
    }
}