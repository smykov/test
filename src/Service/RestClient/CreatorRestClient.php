<?php

namespace App\Service\RestClient;

use App\Service\Creator;

class CreatorRestClient extends Creator
{
    public function __construct(array $dataConnect)
    {
        $this->client = new RestClient($dataConnect);
    }
}