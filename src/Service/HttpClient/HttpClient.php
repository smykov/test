<?php

namespace App\Service\HttpClient;

class HttpClient implements HttpClientInterface
{
    private $url, $login, $password;

    public function __construct(array $dataConnect)
    {
        $this->url = $dataConnect['url'];
        $this->login = $dataConnect['login'];
        $this->password = $dataConnect['password'];
    }

    public function connect()
    {
        return 'connect to http client';
    }

    public function getScheme()
    {
        return 'get scheme http client';
    }
}