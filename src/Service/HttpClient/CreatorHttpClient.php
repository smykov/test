<?php

namespace App\Service\HttpClient;

use App\Service\Creator;

class CreatorHttpClient extends Creator
{
    public function __construct(array $dataConnect)
    {
        $this->client = new HttpClient($dataConnect);
    }
}