<?php

namespace App\Service;

abstract class Creator implements CreatorInterface
{
    /** @var ClientInterface */
    protected $client;

    public function getClientScheme()
    {
        $result[] = $this->client->connect();
        $result[] = $this->client->getScheme();

        return implode('<br />', $result);
    }
}