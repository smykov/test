<?php

namespace App\Service\GrpcClient;

class GrpcClient implements GrpcClientInterface
{
    private $url, $login, $password;

    public function __construct(array $dataConnect)
    {
        $this->url = $dataConnect['url'];
        $this->login = $dataConnect['login'];
        $this->password = $dataConnect['password'];
    }

    public function connect()
    {
        return 'connect to grps client';
    }

    public function getScheme()
    {
        return 'get scheme grps client';
    }
}