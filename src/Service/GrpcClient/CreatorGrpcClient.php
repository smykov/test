<?php

namespace App\Service\GrpcClient;

use App\Service\Creator;

class CreatorGrpcClient extends Creator
{
    public function __construct(array $dataConnect)
    {
        $this->client = new GrpcClient($dataConnect);
    }
}