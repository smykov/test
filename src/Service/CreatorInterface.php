<?php

namespace App\Service;

interface CreatorInterface
{
    public function getClientScheme();
}