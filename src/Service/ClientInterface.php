<?php

namespace App\Service;

interface ClientInterface
{
    public function connect();

    public function getScheme();
}