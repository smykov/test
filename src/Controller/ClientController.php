<?php


namespace App\Controller;

use App\Factory\ClientFactoryInterface;
use App\Service\GrpcClient\CreatorGrpcClient;
use App\Service\HttpClient\CreatorHttpClient;
use App\Service\RestClient\CreatorRestClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("service", name="service_")
 */
class ClientController extends AbstractController
{
    private $factory;

    public function __construct(ClientFactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @Route(name="index", methods={"GET"})
     */
    public function index()
    {
        $grpc = new CreatorGrpcClient($this->factory->getConfigByService('grpc'));
        $http = new CreatorHttpClient($this->factory->getConfigByService('http'));
        $rest = new CreatorRestClient($this->factory->getConfigByService('rest'));

        $output = '<br />gRPC scheme:<br />';
        $output .= $this->factory->getClientScheme($grpc);
        $output .= '<br />HTTP scheme:<br />';
        $output .= $this->factory->getClientScheme($http);
        $output .= '<br />REST scheme:<br />';
        $output .= $this->factory->getClientScheme($rest);

        return new Response(
            '<html><body>' . $output . '</body></html>'
        );
    }

}