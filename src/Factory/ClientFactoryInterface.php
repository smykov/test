<?php

namespace App\Factory;

use App\Service\CreatorInterface;

interface ClientFactoryInterface
{
    public function getClientScheme(CreatorInterface $creator);

    public function getConfigByService(string $type);
}