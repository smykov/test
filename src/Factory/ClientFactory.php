<?php

namespace App\Factory;

use App\Service\CreatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ClientFactory implements ClientFactoryInterface
{
    private $configuration;

    public function __construct(ParameterBagInterface $params)
    {
        $this->configuration = $params->get('factoryClients');
    }

    public function getClientScheme(CreatorInterface $creator)
    {
        return $creator->getClientScheme();
    }

    public function getConfigByService(string $type)
    {
        return $this->configuration[$type];
    }
}